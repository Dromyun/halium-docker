FROM debian:9.9

ENV USER="user"
ENV GROUP="user"
ENV HOME_DIR="/home/${USER}"
ENV WORK_DIR="${HOME_DIR}/syncsrc" \
    SRC_DIR="${HOME_DIR}/src" \
    PATH="${HOME_DIR}/.local/bin:${PATH}"

RUN dpkg --add-architecture i386
RUN apt-get update -qq > /dev/null
# configures locale
RUN DEBIAN_FRONTEND=noninteractive apt-get install -qq --yes --no-install-recommends \
    locales \
    && locale-gen en_US.UTF-8
ENV LANG="en_US.UTF-8" \
    LANGUAGE="en_US.UTF-8" \
    LC_ALL="en_US.UTF-8"

# system requirements to build most of the recipes
RUN apt-get install -qq --yes  \
    git gnupg flex bison gperf build-essential \
    zip bzr curl libc6-dev libncurses5-dev:i386 x11proto-core-dev \
    libx11-dev:i386 libreadline-dev:i386 libgl1-mesa-glx:i386 \
    libgl1-mesa-dev g++-multilib mingw-w64-i686-dev tofrodos \
    python-markdown libxml2-utils xsltproc zlib1g-dev:i386 schedtool \
    liblz4-tool bc lzop imagemagick libncurses5 rsync \
    sudo wget \
    2>/dev/null

RUN curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
RUN chmod a+x /usr/local/bin/repo

# prepares non root env
RUN useradd --create-home --shell /bin/bash ${USER}
# with sudo access and no password
RUN usermod -append --groups sudo ${USER}
RUN echo "%sudo ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

USER ${USER}

RUN mkdir -p ${WORK_DIR}

COPY --chown=user:user ./src ${SRC_DIR}

WORKDIR ${SRC_DIR}
RUN mkdir halium && cd halium
RUN repo init -u https://github.com/Halium/android -b halium-7.1 --depth=1

WORKDIR ${WORK_DIR}
